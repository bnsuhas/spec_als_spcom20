This repository contains all the code that was used in the paper titled 'Speech task based automatic classification of ALS
and Parkinson’s Disease and their severity using log
Mel spectrograms' for SPCOM 2020.